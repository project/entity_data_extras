<?php
/**
 * @file
 * Admin page callbacks for the entity_data_extras_plus module.
 */

/**
 * Entity Data Extras Plus config form.
 */
function entity_data_extras_plus_config($form, &$form_state) {
  $form['#cache'] = TRUE;
  $form['item'] = array(
    '#type' => 'markup',
    '#markup' => t('Select content type used with Entity Data Extras Plus'),
  );

  $form['entity_data_extras_plus'] = array(
    '#tree'  => TRUE,
    '#theme' => 'entity_data_extras_plus_table_display',
  );

  $info = entity_get_info();
  $extras_type = array('block', 'node', 'bean', 'taxonomy_term');
  $entity_data_extras_plus = variable_get('entity_data_extras_plus', array());

  $info['block'] = array(
    'label' => 'Block',
    'bundles' => array(
      'block' => array(
        'label' => 'Block'
      )
    ),
  );

  foreach ($extras_type as $item) {
    if (isset($info[$item])) {
      $bundles = $info[$item]['bundles'];
      foreach ($bundles as $key => $item_type) {
        $enabled   = isset($entity_data_extras_plus[$item][$key]['enabled']) ? $entity_data_extras_plus[$item][$key]['enabled'] : FALSE;
        $display   = isset($entity_data_extras_plus[$item][$key]['display']) ? $entity_data_extras_plus[$item][$key]['display'] : 0;
        $css_class = isset($entity_data_extras_plus[$item][$key]['css']) ? $entity_data_extras_plus[$item][$key]['css'] : '';
        $form['entity_data_extras_plus'][$item][$key] = entity_data_extras_plus_config_text_combo($item_type["label"], $enabled, $display, $css_class);
      }
    }
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

/**
 * Create text and combo box elemenet.
 */
function entity_data_extras_plus_config_text_combo($bundle, $enabled = FALSE, $display = 0, $css_class = '') {
  $form['#tree'] = TRUE;

  $form['enabled'] = array(
    '#type'          => 'checkbox',
    '#default_value' => $enabled,
    '#title'         => $bundle,
  );

  $form['display'] = array(
    '#type' => 'select',
    '#options' => array(
      0 => t('Select + Text'),
      1 => t('Select'),
      2 => t('Text'),
    ),
    '#default_value' => $display,
  );

  $form['css'] = array(
    '#type'          => 'textarea',
    '#rows' => 2,
    '#default_value' => $css_class,
    '#element_validate' => array('entity_data_extras_plus_validate_css'),
  );

  return $form;
}

/**
 * Validate key value css data.
 */
function entity_data_extras_plus_validate_css($element, &$form_state, $form) {
  $values = list_extract_allowed_values($element['#value'], 'list_text', FALSE);
  if (!empty($element['#value']) && !is_array($values)) {
    form_error($element, t('Allowed values: invalid input.'));
  }

}

/**
 * Handle entity_data_extras_plus_config submit.
 */
function entity_data_extras_plus_config_submit($form, &$form_state) {
  variable_set('entity_data_extras_plus', $form_state['values']['entity_data_extras_plus']);
}

/**
 * Theme for entity_data_extras_plus_table_display.
 */
function theme_entity_data_extras_plus_table_display($variables) {
  $form = $variables['form'];
  $output = '';
  foreach (element_children($form) as $entity_type) {
    if ($entity_type == 'block') {
      $entity_info['label'] = t('Block Core');
    }
    else if($entity_type == 'bean'){
      $entity_info = entity_get_info($entity_type);
      $entity_info['label'] = t('Block Bean');
    }
    else {
      $entity_info = entity_get_info($entity_type);
    }


    $output .= format_string("<h2>@label</h2>", array('@label' => $entity_info['label']));
    $rows = array();
    foreach (element_children($form[$entity_type]) as $key) {
      // Build the table row.
      $rows[$key] = array(
        'data' => array(
          array('data' => drupal_render($form[$entity_type][$key]['enabled']), 'class' => 'entity-data-extras-plus-enabled'),
          array('data' => drupal_render($form[$entity_type][$key]['display']), 'class' => 'entity-data-extras-plus-display'),
          array('data' => drupal_render($form[$entity_type][$key]['css']), 'class' => 'entity-data-extras-plus-css'),
        ),
      );
      // Add any attributes on the element to the row, such as the ahah class.
      if (array_key_exists('#attributes', $form[$entity_type][$key])) {
        $rows[$key] = array_merge($rows[$key], $form[$entity_type][$key]['#attributes']);
      }
    }
    $header = array(
      array('data' => t('Enabled'), 'title' => t('Flag whether the given override should be active.')),
      array('data' => t('Choose CSS type'), 'title' => t('The display type of css class, Select or Text field.')),
      array('data' => t('Select CSS Class: each class per line with format key|value'), 'title' => t('Config CSS key|value for Select display type.')),
    );

    $output .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
    ));
  }
  $output .= drupal_render_children($form);

  return $output;
}
