<?php
/**
 * @file
 * Entity related functions for Entity Data Extras module.
 */

/**
 * Helper function that returns entity info for all supported core modules.
 */
function entity_data_extras_core_entity_info() {
  $info = array();
/*  $info['user'] = array(
    'base table' => 'users',
    'entity keys' => array(
      'uuid' => 'uuid',
    ),
  );*/
  $info['block'] = array(
    'base table' => 'block',
  );

  $info['node'] = array(
    'base table' => 'node',
    'revision table' => 'node_revision',
  );
  if (module_exists('comment')) {
    $info['comment'] = array(
      'base table' => 'comment',
    );
  }
  if (module_exists('file')) {
    $info['file'] = array(
      'base table' => 'file_managed',
    );
  }
  if (module_exists('taxonomy')) {
    $info['taxonomy_term'] = array(
      'base table' => 'taxonomy_term_data',
    );
  }
  if (module_exists('field_collection')) {
    $info['field_collection_item'] = array(
      'base table' => 'field_collection_item',
    );
  }

  return $info;
}

/**
 * Get supported entitty type.
 *
 * @return mixed
 *   Array of supported entity type.
 */
function entity_data_extras_supported() {
  $supported = &drupal_static(__FUNCTION__, array());
  if (empty($supported)) {
    $entity_types  = array_keys(entity_data_extras_core_entity_info());
    $entity_types[] = 'user';
    $entity_types[] = 'bean';
    drupal_alter('entity_data_extras_supported', $entity_types);
    $supported = $entity_types;
  }

  return $supported;
}

/**
 * Get bean extras data.
 *
 * @param object $entity
 *   The entity.
 * @param string $key
 *   Key name of data.
 * @param mixed $default
 *   Default value.
 *
 * @return mixed
 *   The return data.
 */
function entity_data_extras_get_data($entity, $key, $default = NULL) {
  if (isset($entity->data_extras[$key])) {
    return $entity->data_extras[$key];
  }

  return $default;
}

/**
 * Set bean zmdata.
 *
 * @param object $entity
 *   The entity.
 * @param string $key
 *   Key name of data.
 * @param mixed $value
 *   Value of data.
 */
function entity_data_extras_set_data($entity, $key, $value) {
  // Set data for user.
  if (isset($entity->uid) && isset($entity->roles)) {
    $entity->data[$key] = $value;
  }

  $entity->data_extras[$key] = $value;
}
